﻿namespace Domain
{
    public static class DbInitializer
    {
        public static void Initialize(ChatDbContext context)
        {
            context.Database.EnsureCreated();
            context.SaveChanges();
        }
    }
}
