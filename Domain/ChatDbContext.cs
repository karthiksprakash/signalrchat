﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory.ValueGeneration.Internal;

namespace Domain
{
    public class ChatDbContext : DbContext
    {
        public ChatDbContext(DbContextOptions<ChatDbContext> options) : base(options)
        {
        }

        public DbSet<Session> Sessions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Session>(e =>
            {
                e.Property(m => m.SessionId).ValueGeneratedOnAdd();
                if (Database.IsInMemory())
                    e.Property(m => m.SessionId).HasValueGenerator<InMemoryIntegerValueGenerator<long>>();

                e.Property(m => m.UserId).IsUnicode(false).HasMaxLength(127).IsRequired();
                e.Property(m => m.UserAgent).IsUnicode(false).HasMaxLength(48);
                e.Property(m => m.UserAppId).IsUnicode(false).HasMaxLength(48);
                e.Property(m => m.Origin).IsUnicode(false).HasMaxLength(128);
                e.Property(m => m.IpAddress).IsUnicode(false).HasMaxLength(15);
                e.Property(m => m.UserConnectionId).IsUnicode(false).HasMaxLength(36).IsRequired();
                e.Property(m => m.ConversationId).IsUnicode(false).HasMaxLength(36);
                e.Property(m => m.Created).IsRequired();
                e.Property(m => m.ServiceGroup).IsUnicode(false).HasMaxLength(36);
                e.Property(m => m.ServiceConnectionId).IsUnicode(false).HasMaxLength(36);
            });
        }
    }
}
