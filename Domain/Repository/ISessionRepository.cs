﻿using System.Threading.Tasks;
using Domain.Models;

namespace Domain.Repository
{
    public interface ISessionRepository
    {
        Task<Session> GetLatestAsync(string userId, bool asNoTracking = true);
        Task<Session> GetLatestAsync(string userId, string appId, bool asNoTracking = true);
        Task<Session> GetAsync(string userConnectionId);
        Task<Session> AddAsync(Session session);
        Task<int> RemoveAsync(string userId, string userConnectionId);
        Task<int> SetInActiveAsync(string userId, string userConnectionId);
        Task<int> SaveChangesAsync();
    }
}
