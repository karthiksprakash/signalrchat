﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogic.Models;

namespace BusinessLogic
{
    public interface IChatFacade
    {
        Task<List<ChatModel>> RespondAsync(string connectionId, string message);
        List<ChatModel> GetConversationContinue(string userId, string appId);
        List<ChatModel> GetConversationStart(string userId, string appId);
        List<ChatModel> GetPromptToContinue(string userId, string appId);
    }
}