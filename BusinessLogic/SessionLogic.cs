﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Domain.Models;
using Domain.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Serilog;

namespace BusinessLogic
{
    public class SessionLogic : ISessionLogic
    {
        private readonly ISessionRepository _sessionRepo;
        private readonly ILogger _logger;

        public SessionLogic(ISessionRepository sessionRepository, ILogger logger)
        {
            _sessionRepo = sessionRepository;
            _logger = logger;
        }

        public async Task<string> GetConversationId(string connectionId)
        {
            var session = await _sessionRepo.GetAsync(connectionId);
            return session?.ConversationId;
        }

        public async Task<Session> CreateSessionAsync(HttpContext httpContext, string userId, string appId, string connectionId)
        {
            string ipAddress = httpContext.Connection.RemoteIpAddress.ToString();
            var requestHeaders = httpContext.Request.Headers;

            var userAgent = requestHeaders.ContainsKey(HeaderNames.UserAgent) ? requestHeaders[HeaderNames.UserAgent].FirstOrDefault() : string.Empty;
            var origin = requestHeaders.ContainsKey(HeaderNames.Origin) ? requestHeaders[HeaderNames.Origin].FirstOrDefault() : string.Empty;

            var session = new Session()
            {
                UserId = userId,
                UserConnectionId = connectionId,
                UserAgent = string.Empty,
                UserAppId = appId,
                Origin = origin,
                IpAddress = ipAddress,
                ServiceId = string.Empty,
                ServiceConnectionId = string.Empty,
                ServiceGroup = string.Empty,
                Created = DateTime.UtcNow,
                Modified = DateTime.UtcNow,
                IsActive = true
            };
            return await _sessionRepo.AddAsync(session);
        }

        public async Task<Tuple<Session, bool>> InitAsync(HttpContext httpContext, string userId, string appId, string connectionId)
        {
            bool shouldStartConversation = false;

            //Check if there is an existing current session
            var session = await _sessionRepo.GetLatestAsync(userId, appId);
            if (!session.IsConversationCurrent)
            {
                //Create new session
                if (!string.IsNullOrEmpty(session.ConversationId))
                {
                    //Session was not created recently (ex. on connect). So create a new one.
                    session = await CreateSessionAsync(httpContext, userId, appId, connectionId);
                }

                await InitSessionWithNewConversationAsync(session); // Saves session to DB
                shouldStartConversation = true;
            }

            return new Tuple<Session, bool>(session, shouldStartConversation);
        }

        /// <summary>
        /// 1. Create new session
        /// 2. Init session with new converasation Id
        /// Saves changes
        /// </summary>
        public async Task<Session> InitNewConversationAsync(HttpContext httpContext, string userId, string appId, string connectionId)
        {
            var session = await CreateSessionAsync(httpContext, userId, appId, connectionId);
            await InitSessionWithNewConversationAsync(session);
            return session;
        }


        /// <summary>
        /// Init session with new converasation Id
        /// Saves changes
        /// </summary>
        public async Task InitSessionWithNewConversationAsync(Session session)
        {
            session.IsConversationCurrent = true;
            session.ConversationId = Guid.NewGuid().ToString();
            session.Modified = DateTime.UtcNow;
            await _sessionRepo.SaveChangesAsync();
        }

    }
}
