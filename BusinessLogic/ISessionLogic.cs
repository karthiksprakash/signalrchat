﻿using System;
using System.Threading.Tasks;
using Domain.Models;
using Microsoft.AspNetCore.Http;

namespace BusinessLogic
{
    public interface ISessionLogic
    {
        Task<Tuple<Session, bool>> InitAsync(HttpContext httpContext, string userId, string appId, string connectionId);
        Task<Session> CreateSessionAsync(HttpContext httpContext, string userId, string appId, string connectionId);
        Task<Session> InitNewConversationAsync(HttpContext httpContext, string userId, string appId, string connectionId);
        Task InitSessionWithNewConversationAsync(Session session);
        Task<string> GetConversationId(string connectionId);
    }
}