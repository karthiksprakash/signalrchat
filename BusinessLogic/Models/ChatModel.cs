﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Models
{
    public class ChatModel
    {
        public string Author { get; set; }
        public ChatDataType Type { get; set; }
        public Data Data { get; set; }
    }

    public class Data
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public string Html { get; set; }
        public string Name { get; set; }
        public string Glyphicon { get; set; }
    }

    public enum ChatDataType
    {
        Text,
        Image,
        Video,
        File
    }
}
