﻿using System.Threading.Tasks;

namespace BusinessLogic.Services.Rasa
{
    public interface IRasaCore
    {
        Task<dynamic> GetVersionAsync();
        Task<dynamic> RespondAsync(string senderId, string message);
    }
}