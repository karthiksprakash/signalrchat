﻿using System;
using System.Threading.Tasks;
using RestSharp;
using Serilog;

namespace BusinessLogic.Services.Rasa
{
    public class RasaCore : IRasaCore
    {
        private readonly string _url;
        private readonly ILogger _logger;
        private readonly RestClient _restClient;

        public RasaCore(string url, ILogger logger)
        {
            _url = url;
            _logger = logger;
            _restClient = new RestClient(url);
        }
        
        public async Task<dynamic> GetVersionAsync()
        {
            var request = new RestRequest("version");

            return (await GetResponse<dynamic>(request))?.Data;
        }

        public async Task<dynamic> RespondAsync(string senderId, string message)
        {
            ///webhooks/rest/webhook
            //var request = new RestRequest($"conversations/{senderId}/respond", Method.POST) {RequestFormat = DataFormat.Json};
            var request = new RestRequest("webhooks/rest/webhook", Method.POST) { RequestFormat = DataFormat.Json };
            var body = new {sender = senderId, message};
            request.AddBody(body);

            return (await GetResponse<dynamic>(request))?.Data;
        }

        private async Task<IRestResponse<T>> GetResponse<T>(RestRequest request)
        {
            try
            {
                var response = await _restClient.ExecuteTaskAsync<dynamic>(request);
                return response.Data;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Unexpected error calling RASA endpoint.");
                return null;
            }
        }

        //private ResponseModel<T> GetResponseModel<T>(IRestResponse<T> restResponse)
        //{
        //    return new ResponseModel<T>()
        //    {
        //        IsSuccess = restResponse.IsSuccessful,
        //        Data = restResponse.Data,// != null? (T) Newtonsoft.Json.JsonConvert.DeserializeObject(restResponse.Data.ToString()) : default(T),
        //        Message = !restResponse.IsSuccessful? $"Error communicating with RASA. {restResponse.StatusCode} - {restResponse.ErrorMessage}" : string.Empty
        //    };
        //}
    }
}
