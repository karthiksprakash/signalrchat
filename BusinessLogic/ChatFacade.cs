﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Models;
using BusinessLogic.Services.Rasa;
using Domain.Repository;

namespace BusinessLogic
{
    public class ChatFacade : IChatFacade
    {
        private readonly ISessionLogic _sessionLogic;
        private readonly IRasaCore _rasaCore;

        public ChatFacade(ISessionLogic sessionLogic, IRasaCore rasaCore)
        {
            _sessionLogic = sessionLogic;
            _rasaCore = rasaCore;
        }

        public async Task<List<ChatModel>> RespondAsync(string connectionId, string message)
        {
            var response = new List<ChatModel>();

            string conversationId = await _sessionLogic.GetConversationId(connectionId);
            var rasaResponse = await _rasaCore.RespondAsync(conversationId, message);

            response.Add(rasaResponse == null
                ? GetTextChat("D'oh! Something went wrong...")
                : (ChatModel) GetTextChat(rasaResponse.text));

            return response;
        }

        public List<ChatModel> GetPromptToContinue(string userId, string appId)
        {
            return new List<ChatModel>()
            {
                GetTextChat("Nice to see you back! Would you like to continue where we left off?")
            };
        }

        public List<ChatModel> GetConversationStart(string userId, string appId)
        {
            return new List<ChatModel>()
            {
                GetTextChat("Hello! I'm Mobot, your personal assistant."),
                GetTextChat("How can I help you?")
            };
        }

        public List<ChatModel> GetConversationContinue(string userId, string appId)
        {
            return new List<ChatModel>()
            {
                GetTextChat("Rasa", "Previous conversation.")
            };
        }

        private ChatModel GetTextChat(string text, string author = "rasa")
        {
            return new ChatModel() {Author = author, Type = ChatDataType.Text, Data = new Data {Text = text}};
        }
    }
}
