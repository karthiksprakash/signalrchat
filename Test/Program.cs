﻿using System;
using BusinessLogic.Services.Rasa;
using Serilog;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.Console().CreateLogger();

            RasaCore rasa = new RasaCore("http://karthik-ws:5001/", Log.Logger);
            //var result = rasa.RespondAsync("default", "hi").GetAwaiter().GetResult();
            var result = rasa.GetVersionAsync().GetAwaiter().GetResult();
        }
    }
}
