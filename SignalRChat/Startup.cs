﻿using System.Reflection;
using BusinessLogic.Services.Rasa;
using Domain;
using Domain.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Sinks.Graylog;
using SignalRChat.Hubs;
using Swashbuckle.AspNetCore.Swagger;

namespace SignalRChat
{
    public class Startup
    {
        private readonly string _version = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>()
            .InformationalVersion;
        private readonly string _appName = Assembly.GetExecutingAssembly().GetName().Name;

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(CreateLogger(Configuration));
            services.AddDbContext<ChatDbContext>(options =>
            {
                var connectionString = Configuration["ConnectionStrings:ChatDb"];
                options.UseMySql(connectionString);
            });
            services.AddScoped<ISessionRepository, SessionRepository>();
            services.AddTransient<IRasaCore>(s => new RasaCore(Configuration["Rasa:CoreUrl"], s.GetService<ILogger>()));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "SignalRChat", Version = "v1" });
            });

            services.AddSignalR(hubOptions =>
            {
                //Send error details to client if in development.
                if (Environment.IsDevelopment())
                    hubOptions.EnableDetailedErrors = true;
            }).AddMessagePackProtocol();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            ConfigureCors(services);
        }
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors("AllowAllCorsPolicy");
            }
            else
            {
                app.UseHsts();
                app.UseCors("ProdAllCorsPolicy");
            }

            //app.UseHttpsRedirection();
            app.UseWelcomePage("/welcome");
            
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{_appName} : {_version}");
            });
            app.UseSignalR(routes =>
            {
                routes.MapHub<RasaHub>("/rasaHub");
            });
            app.UseMvc();
        }
        

        public static ILogger CreateLogger(IConfiguration configuration)
        {
            return new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.Graylog(new Serilog.Sinks.Graylog.Core.GraylogSinkOptions
                {
                    HostnameOrAddress = configuration["Graylog:Host"],
                    Port = int.Parse(configuration["Graylog:Port"]),
                    MinimumLogEventLevel = Serilog.Events.LogEventLevel.Debug,
                    Facility = configuration["ASPNETCORE_ENVIRONMENT"]
                })
                .CreateLogger();
        }

        private void ConfigureCors(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllCorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            services.AddCors(options =>
            {
                options.AddPolicy("ProdCorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
        }
    }
}
