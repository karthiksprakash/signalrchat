﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Domain.Models
{
    public class Session
    {
        public long SessionId { get; set; }
        public string UserId { get; set; }
        public string UserConnectionId { get; set; }
        public string UserAgent { get; set; }
        public string UserAppId { get; set; }
        public string ConversationId { get; set; }
        public string Origin { get; set; }
        public string IpAddress { get; set; }
        public string ServiceGroup { get; set; }
        public string ServiceId { get; set; }
        public string ServiceConnectionId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsActive { get; set; }
        public bool IsConversationCurrent { get; set; }
    }
}
