﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Domain
{
    public static class DbInitializer
    {
        public static void Initialize(ChatDbContext context)
        {
            context.Database.EnsureCreated();
            context.SaveChanges();
        }
    }
}
