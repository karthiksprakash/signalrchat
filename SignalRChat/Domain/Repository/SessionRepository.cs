﻿using SignalRChat.Domain.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SignalRChat.Domain.Repository
{
    class SessionRepository : ISessionRepository
    {
        private readonly ChatDbContext _dbContext;

        public SessionRepository(ChatDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Session> GetAsync(string userConnectionId)
        {
            return await _dbContext.Sessions.FirstOrDefaultAsync(s => s.UserConnectionId == userConnectionId);
        }

        public async Task<Session> AddAsync(Session session)
        {
            await _dbContext.Sessions.AddAsync(session);
            await _dbContext.SaveChangesAsync();
            return session;
        }
        
        public async Task<Session> GetLatestAsync(string userId, bool asNoTracking = true)
        {
            string userIdLowerCase = userId.ToLower();
            var sessionsQuery = asNoTracking ? _dbContext.Sessions.AsNoTracking() : _dbContext.Sessions;
            var session = await sessionsQuery.Where(s => s.UserId == userIdLowerCase).OrderByDescending(s => s.Modified)
                .FirstOrDefaultAsync();
            return session;
        }


        public async Task<Session> GetLatestAsync(string userId, string appId, bool asNoTracking = true)
        {
            string userIdLowerCase = userId.ToLower();
            var sessionsQuery = asNoTracking ? _dbContext.Sessions.AsNoTracking() : _dbContext.Sessions;
            var session = await sessionsQuery.Where(s => s.UserId == userIdLowerCase && s.UserAppId == appId)
                .OrderByDescending(s => s.Modified).FirstOrDefaultAsync();
            return session;
        }


        public async Task<int> RemoveAsync(string userId, string userConnectionId)
        {
            var session = _dbContext.Sessions.AsNoTracking().FirstOrDefault(x => x.UserId == userId && x.UserConnectionId == userConnectionId);
            if (session != null)
            {
                _dbContext.Sessions.Remove(session);
                return await _dbContext.SaveChangesAsync();
            }

            return 0;
        }

        public async Task<int> SetInActiveAsync(string userId, string userConnectionId)
        {
            var session = _dbContext.Sessions.FirstOrDefault(x => x.UserId == userId && x.UserConnectionId == userConnectionId);
            if (session != null)
            {
                session.IsActive = false;
                return await _dbContext.SaveChangesAsync();
            }

            return 0;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
    }
}