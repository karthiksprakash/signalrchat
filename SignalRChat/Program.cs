﻿using System;
using Domain;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Sinks.Graylog;

namespace SignalRChat
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var host = CreateWebHostBuilder(args).Build();
            Log.Logger = Startup.CreateLogger(host.Services.GetRequiredService<IConfiguration>());

            using (var scope = host.Services.CreateScope())
            {
                try
                {
                    var context = scope.ServiceProvider.GetRequiredService<ChatDbContext>();
                    DbInitializer.Initialize(context);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "An error occurred while seeding the database.");
                }
            }

            try
            {
                Log.Information("Starting the web host...");
                host.Run();
            }
            catch(Exception e)
            {
                Log.Fatal(e, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, config) =>
                {
                    var builtConfig = config.Build();
                    if (builtConfig["ASPNETCORE_ENVIRONMENT"] != EnvironmentName.Development) //Avoid Azure Key-Value for local development. Use secrets.json
                    {
                        var keyVaultConfigBuilder = new ConfigurationBuilder();

                        keyVaultConfigBuilder.AddAzureKeyVault(
                            $"https://{builtConfig["Vault"]}.vault.azure.net/",
                            builtConfig["ClientId"],
                            builtConfig["ClientSecret"]);

                        var keyVaultConfig = keyVaultConfigBuilder.Build();

                        config.AddConfiguration(keyVaultConfig);
                    }
                })
                //.ConfigureLogging((context, builder) =>
                //{
                //    // Read GelfLoggerOptions from appsettings.json
                //    builder.Services.Configure<GelfLoggerOptions>(context.Configuration.GetSection("Graylog"));

                //    // Optionally configure GelfLoggerOptions further.
                //    builder.Services.PostConfigure<GelfLoggerOptions>(options =>
                //        options.AdditionalFields["machine_name"] = Environment.MachineName);

                //    // Read Logging settings from appsettings.json and add providers.
                //    builder.AddConfiguration(context.Configuration.GetSection("Logging"))
                //        .AddConsole()
                //        .AddDebug()
                //        .AddGelf();

                //    builder.AddFilter("Microsoft.EntityFrameworkCore.Database.Command", LogLevel.Warning);
                //})
                .UseStartup<Startup>()
                .UseSerilog();
        
    }
}
