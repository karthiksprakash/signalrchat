﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Services.Rasa;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using SignalRChat.Hubs;
using SignalRChat.Infrastructure;

namespace SignalRChat.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RasaController : ControllerBase
    {
        
        private readonly IHubContext<RasaHub> _hubContext;
        private readonly IRasaCore _rasaCore;
        private readonly ILogger<RasaController> _logger;

        public RasaController(IHubContext<RasaHub> hubContext, IRasaCore rasaCore, ILogger<RasaController> logger)
        {
            _hubContext = hubContext;
            _rasaCore = rasaCore;
            _logger = logger;
        }

        [HttpGet(Name = "RasaDetails")]
        public async Task<dynamic> GetRasaDetailsAsync()
        {
            try
            {
                return await _rasaCore.GetVersionAsync();
            }
            catch (Exception e)
            {
                string message = $"{nameof(GetRasaDetailsAsync)}";
                _logger.LogError(e, message);
                return GetResponseModel<dynamic>(null, HttpStatusCode.InternalServerError, message);
            }
        }
        
        [HttpPost("/respond", Name = "RasaRespond")]
        public async Task<dynamic> RasaRespond([FromBody] dynamic message)
        {
            try
            {
                return await _rasaCore.RespondAsync((string)message.sender_id, (string)message.query);
            }
            catch (Exception e)
            {
                string err = $"{nameof(RasaRespond)}. ClientId = {(string)message.sender_id}, query = {(string)message.query}.";
                _logger.LogError(e, err);
                return GetResponseModel<dynamic>(null, HttpStatusCode.InternalServerError, err);
            }
        }

        [HttpPost("/client/{clientId}", Name = "SendToClient")]
        public async Task SendMessageToClient(string clientId, [FromBody] dynamic message)
        {
            try
            {
                await RasaHub.SendMessageToClient(_hubContext, clientId, message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{nameof(SendMessageToClient)}. ClientId = {clientId}");
            }
        }


        private ResponseModel<T> GetResponseModel<T>(T data, HttpStatusCode httpStatus = HttpStatusCode.OK, string message = "")
        {
            return new ResponseModel<T>()
            {
                Message = message,
                Data = data,
                IsSuccess = string.IsNullOrWhiteSpace(message)
            };
        }
    }
}
