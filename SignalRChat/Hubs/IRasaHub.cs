﻿using System.Threading.Tasks;

namespace SignalRChat.Hubs
{
    public interface IRasaHub
    {
        Task<dynamic> GetRasaDetails(string connectionId);
        Task Init(string userId, string appId);
        Task InitNew(string userId, string appId);
        Task<dynamic> Respond(string message);
        //bool ReturnClientDetails(dynamic clientDetails);
        Task SendMessage(string connectionId, object message);
    }
}