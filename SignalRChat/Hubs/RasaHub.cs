﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic;
using BusinessLogic.Services.Rasa;
using Domain.Models;
using Domain.Repository;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

namespace SignalRChat.Hubs
{
    public class RasaHub : HubBase<RasaHub>, IRasaHub
    {
        private const int ResultSleepTime = 20;
        private readonly IChatFacade _chatFacade;
        private readonly ISessionLogic _sessionLogic;
        private readonly ILogger<RasaHub> _logger;
        private static IRasaCore _rasaCore;
        
        
        public RasaHub(IChatFacade chatFacade, ISessionLogic sessionLogic, ISessionRepository sessionRepository, IRasaCore rasaCore, ILogger<RasaHub> logger) : base(sessionLogic, sessionRepository, logger)
        {
            _chatFacade = chatFacade;
            _sessionLogic = sessionLogic;
            _logger = logger;
            if (_rasaCore == null)
                _rasaCore = rasaCore;
        }
        

        public async Task SendMessage(string connectionId, object message)
        {
            await Clients.Client(connectionId).SendAsync("on_message", message);
        }

        public async Task SendBulkMessage(string connectionId, object message)
        {
            await Clients.Client(connectionId).SendAsync("on_bulk_message", message);
        }

        public async Task<dynamic> GetRasaDetails(string connectionId)
        {
            return await _rasaCore.GetVersionAsync();
        }

        public async Task Init(string userId, string appId)
        {
            //Check if there is an existing current session
            Tuple<Session, bool> result = await _sessionLogic.InitAsync(HttpContext, userId, appId, Context.ConnectionId);
            
            if (result.Item2)
            {
                //Start fresh conversation
                await StartConversation(result.Item1, userId, appId);
            }
            else
            {
                //Ask user if he wants to continue
                await PromptToContinue(userId, appId);
            }
        }

        public async Task InitNew(string userId, string appId)
        {
            Session session = await _sessionLogic.InitNewConversationAsync(HttpContext, userId, appId, Context.ConnectionId); // Saves session to DB

            // Start conversation?
            await StartConversation(session, userId, appId);
        }

        public async Task<dynamic> Respond(string message)
        {
            return await _chatFacade.RespondAsync(Context.ConnectionId, message);
        }
        

        #region Helpers

        private async Task PromptToContinue(string userId, string appId)
        {
            //Show message
            foreach (var chat in _chatFacade.GetPromptToContinue(userId, appId))
            {
                await SendMessage(Context.ConnectionId, chat);
            }

            //Show buttons

        }

        private async Task StartConversation(Session session, string userId, string appId)
        {
            foreach (var chat in _chatFacade.GetConversationStart(userId, appId))
            {
                await SendMessage(Context.ConnectionId, chat);
            }
        }
        
        private async Task ContinueConversation(Session session, string userId, string appId)
        {
            //Get data from RASA and push them to app
            await SendBulkMessage(Context.ConnectionId, _chatFacade.GetConversationContinue(userId, appId));
        }


        // block until the requested key is populated in the dictionary
        // return a default value if something goes wrong and the client does not push the value within the timeout period
        // or if the server has ordered a shutdown before the value is returned
        private static async Task<T> GetResultAsync<T, TKey>(TKey key, ConcurrentDictionary<TKey, T> dataStore, int timeOutMs = 1000)
        {
            int i = 0;
            while (!dataStore.ContainsKey(key) && i < timeOutMs)
            {
                await Task.Delay(ResultSleepTime);
                i = i + ResultSleepTime;
            }

            if (dataStore.ContainsKey(key))
            {
                dataStore.TryRemove(key, out var result);
                return result;
            }
            return default(T);
        }

        private bool TryStoreReturnedResult<T>(ConcurrentDictionary<string, T> dataStore, T value)
        {
            var result = false;
            if (!dataStore.ContainsKey(Context.ConnectionId))
                result = dataStore.TryAdd(Context.ConnectionId, value);

            return result;
        }

        #endregion


        #region Static methods

        public static async Task SendMessageToClient(IHubContext<RasaHub> hubContext, string userId, object message)
        {
            await hubContext.Clients.Client(userId).SendAsync("on_message", message);
        }

        #endregion
    }
}

