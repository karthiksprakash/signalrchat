﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic;
using Domain.Models;
using Domain.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections.Internal;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

namespace SignalRChat.Hubs
{
    public class HubBase<T> : Hub
    {
        private readonly ISessionLogic _chatLogic;
        private readonly ISessionRepository _sessionRepository;
        private readonly ILogger<T> _logger;

        public HubBase(ISessionLogic chatLogic, ISessionRepository sessionRepository, ILogger<T> logger)
        {
            _chatLogic = chatLogic;
            _sessionRepository = sessionRepository;
            _logger = logger;
        }

        private HttpContext _httpContext;

        public HttpContext HttpContext
        {
            get
            {
                if (_httpContext == null)
                {
                    var httpConnectionContext = (HttpConnectionContext) Context.Features.FirstOrDefault(x =>
                            x.Key.FullName.Equals("Microsoft.AspNetCore.Http.Connections.Features.IHttpContextFeature"))
                        .Value;
                    _httpContext = httpConnectionContext.HttpContext;
                }

                return _httpContext;
            }
        }

        #region HUB connection handlers

        public override async Task OnConnectedAsync()
        {
            try
            {
                string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString();
                string userId = null;
                if (HttpContext.Request.Query.ContainsKey("userid"))
                    userId = HttpContext.Request.Query["userid"].FirstOrDefault()?.ToLower();

                string appId = null;
                if (HttpContext.Request.Query.ContainsKey("appid"))
                    appId = HttpContext.Request.Query["appid"].FirstOrDefault()?.ToLower();

                if (string.IsNullOrWhiteSpace(userId))
                    throw new InvalidOperationException("UserId cannot be null or empty");

                if (string.IsNullOrWhiteSpace(appId))
                    throw new InvalidOperationException("AppId cannot be null or empty");

                var session = await _sessionRepository.GetLatestAsync(userId, appId, false);
                if (session == null || !session.IsConversationCurrent && !string.IsNullOrEmpty(session.ConversationId))
                {
                    //New user or users with new app Id, or with a new conversation
                    session = await _chatLogic.CreateSessionAsync(HttpContext, userId, appId, Context.ConnectionId);
                }
                else
                {
                    //Existing user with same IP and current conversation
                    session.UserConnectionId = Context.ConnectionId;
                    session.IpAddress = ipAddress;
                    session.Modified = DateTime.UtcNow;
                }
                await _sessionRepository.SaveChangesAsync();

                _logger.LogInformation($"Connect: User {userId}/{ipAddress} from '{session.Origin}'.");
            }
            catch (Exception e)
            {
                _logger.LogError(e, string.Empty);
                throw;
            }

            await base.OnConnectedAsync();
        }

        

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString();
                var requestHeaders = HttpContext.Request.Headers;
                var origin = requestHeaders.ContainsKey(HeaderNames.Origin) ? requestHeaders[HeaderNames.Origin].FirstOrDefault() : string.Empty;

                string userId = null;
                if (HttpContext.Request.Query.ContainsKey("userid"))
                    userId = HttpContext.Request.Query["userid"].FirstOrDefault()?.ToLower();

                var existingSession = await _sessionRepository.GetAsync(Context.ConnectionId);
                if (existingSession != null)
                {
                    existingSession.IsActive = false;
                    await _sessionRepository.SaveChangesAsync();
                }

                _logger.LogInformation($"Disconnect: User {userId}/{ipAddress} from '{origin}'.");
            }
            catch (Exception e)
            {
                _logger.LogError(e, string.Empty);
                throw;
            }
        }

        #endregion
    }
}
